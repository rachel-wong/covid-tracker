import React, { useState, useEffect } from 'react'
import { NativeSelect, FormControl } from '@material-ui/core'
import { fetchCountries } from '../../api'
import styles from './CountryPicker.module.css'

// the selected "Country" will be an app-level (not component-level) state
// because it affects both the chart and the cards components
const CountryPicker = ({ handleCountry }) => {

  const [countries, setCountries] = useState([])

  useEffect(() => {
    const fetchedCountries = async () => {
      const { data } = await fetchCountries();
      setCountries(data.countries)
    }

    fetchedCountries()
  }, [])

  return (
    <FormControl className={ styles.formControl }>
      <NativeSelect defaultValue="" onChange={(e) => handleCountry(e.target.value)} variant="filled">
        <option value="">Global</option>
        {countries.length && countries.map((country, idx) => {
          return <option key={idx} value={country.name} className={ styles.option} >{ country.name }</option>
        }
        )}
      </NativeSelect>
    </FormControl>
  )
}

export default CountryPicker

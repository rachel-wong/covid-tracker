import React from 'react'
import { Card, CardContent, CardActionArea, Typography, Grid } from '@material-ui/core'
import styles from './Cards.module.css';
import CountUp from 'react-countup'
import classnames from 'classnames'

const Cards = ({ data: { confirmed, recovered, deaths, lastUpdate } }) => {

  const newObj = {confirmed, recovered, deaths}
  /* this handles the loading error */
  if (!confirmed) {
    return 'Loading..'
  }

  // helper function to loop over an object
  function mapObject(object, callback) {
    return Object.keys(object).map(function (key) {
      return callback(key, object[key]);
    });
  }

  return (
    <div className={styles.container}>
      <Grid container spacing={3} justify="center">
        {mapObject(newObj, function (name, value) {
          return <Grid item component={Card} xs={12} md={3} className={ classnames(styles.card, styles.catClass)} key={name}>
            <CardActionArea>
              <CardContent>
                <Typography variant="h4" color="textSecondary" gutterBottom>{name}</Typography>
                <Typography variant="h3"><CountUp start={ 0 } end={value.value} duration={ 2.75 } separator=","/></Typography>
                {/* Key: {name}, Value: {value.value} */}
                <Typography className={ styles.cardLabel } variant="body2">as of {new Date(lastUpdate).toDateString()}</Typography>
                </CardContent>
            </CardActionArea>
            </Grid>
        })}
      </Grid>
    </div>
  )
}

export default Cards
import React, {useState, useEffect } from 'react'
import { fetchDailyData } from '../../api'
import { Line, Bar } from 'react-chartjs-2'

import styles from './Chart.module.css'

const Chart = ({ country, countryData }) => {

  // component level state for GLOBAL data so that api calls can be maintained separately
  const [globalData, setGlobalData] = useState([])

  useEffect(() => {
    const fetchedData = async () => {
      let { data } = await fetchDailyData()
      setGlobalData(data)
    }
    fetchedData()
  }, [])

  const lineOptions = {
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
        },
      }],
      yAxes: [{
        gridLines: {
          display: true,
        },
        ticks: {
          beginAtZero: true,
          callback: function (value, index, values) {
            return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
          }
        },
      }],
    },
    legend: {
      display: true,
    },
    tooltips: {
      enabled: false,
    },
    title: {
      display: true,
      text: 'Daily numbers'
    }
  };

  const barChartOptions = {
    title:{
      display:true,
      text: 'COVID numbers for ' + country
    },
    legend:{
      display:false
    }
  }

  // no daily tracking on recovered stats - excluded from linechart
  const lineDataset = {
    labels: globalData.map(({ reportDate }) => reportDate),
    datasets: [
      {
        label: 'Infected',
        lineTension: 0.1,
        borderColor: 'blue',
        fill: true,
        data: globalData.map(({ confirmed }) => confirmed.total)
      },
      {
        label: 'Deaths',
        lineTension: 0.1,
        borderColor: 'red',
        fill: true,
        data: globalData.map(({ deaths }) => deaths.total)
      }
    ]
  }

  const barChart = (
    countryData.confirmed ? (
      <Bar data={{
        labels: ['Infected', 'Recovered', 'Deaths'],
        datasets: [{
          label: 'People Count',
          backgroundColor: [
            'rgba(255, 0 , 0, 0.5)',
            'rgba(0, 0, 255, 0.5)',
            'rgba(0, 255, 0, 0.5)'
          ],
          data: [countryData.confirmed.value, countryData.recovered.value, countryData.deaths.value]
        }]
       }} options = { barChartOptions } />
    ) : null
  )

  const lineChart = (
    <Line data={lineDataset} options={ lineOptions } />
  )

  return (
    <div className={ styles.container }>
      <div className={ styles.linechart }>
        { country ? barChart : lineChart }
      </div>
    </div>
  )
}

export default Chart

import './App.css';
import React, { useEffect, useState } from 'react';
import styles from './App.module.css';

/* this saves repeating individual imports */
import { Cards, Chart, CountryPicker } from './components'
import { fetchData } from './api' /* when the file is index.js no need to specify it in the path */

// change to functional component instead of class in order to use hooks
function App() {
  const [cardData, setCardData] = useState({});
  const [country, setCountry] = useState("");

  useEffect(() => {
    const fetchedData = async () => {
      const card_response = await fetchData()
      setCardData(card_response.data)
    }
    fetchedData()
  }, [])

  const handleCountry = async (country) => {
    const response = await fetchData(country)
    setCardData(response.data)
    setCountry(country)
  }

  return (
    <div className={styles.container}>
      <CountryPicker handleCountry={handleCountry} />
      <Cards
        data={ cardData } />
      <Chart country={ country } countryData={ cardData }/>
    </div>
  )
}

export default App;

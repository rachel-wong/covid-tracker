import axios from 'axios'

const url = 'https://covid19.mathdro.id/api'

export const fetchData = async (country) => {
  let apiURL = url

  if (country) {
    apiURL += '/countries/' + country
  }

  try {
    const response = await axios.get(apiURL)
    return response
  } catch (error) {
    console.error(error)
    console.log("Bad country code: ", country)
  }
}

export const fetchDailyData = async () => {
  try {
    const response = await axios.get(url + "/daily")
    return response
  } catch (error) {
    console.error(error)
  }
}

export const fetchCountries = async () => {
  try {
    const response = await axios.get(url + '/countries')
    return response
  } catch (error) {
    console.error(error)
  }
}
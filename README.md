### :beetle: Covid Tracker

**Dependencies**

* axios
* react-chartjs-2
* chart.js
* react-countup
* classnames
* @material-ui/core

#### Notes

* Having separate stylesheet in each of the components labelled as `Component.module.css` means that the css will only apply to the component folder and won't affect any other components in the project

* Instead of having repeated individual imports of components

1. create a separate index.js inside components with
`export { default as Chart } from './Chart/Chart'`
2. In App.js, link to to the entire components folder with
`import { Cards, Chart, CountryPicker } from './components'`

* Styles can be centralised and imported as javascript objects.
  Create style in base of `src` as `App.module.css`
  Import it into `App.js` as `import styles from './App.module.css`
  Use it in the jsx as a javascript object like `className={ styles.someClass }`

* When multiple css classes should be applied to the component using modular css as above, use the `classnames` package.

```html
import classnames from 'classnames';

<div className={classnames(styles.class1, styles.class2)}>
```

 * MaterialUI requires `!important` to enforce modular css rules.

* centralize all the possible data calls into one `api/index.js`